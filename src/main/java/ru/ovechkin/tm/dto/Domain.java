package ru.ovechkin.tm.dto;

import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.entity.User;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public final class Domain implements Serializable {

    private List<Project> projects = new ArrayList<>();

    private List<Task> tasks = new ArrayList<>();

    private List<User> users = new ArrayList<>();

    public List<Project> getProjects() {
        if (projects == null) {
            List<Project> projects = new ArrayList<>();
            return projects;
        }
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<Task> getTasks() {
        if (tasks == null) {
            List<Task> tasks = new ArrayList<>();
            return tasks;
        }
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public List<User> getUsers() {
        if (users == null) {
            List<User> users = new ArrayList<>();
            return users;
        }
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

}