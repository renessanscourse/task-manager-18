package ru.ovechkin.tm.bootstrap;

import ru.ovechkin.tm.api.repository.IProjectRepository;
import ru.ovechkin.tm.api.repository.ITaskRepository;
import ru.ovechkin.tm.api.repository.IUserRepository;
import ru.ovechkin.tm.api.service.*;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.exeption.empty.CommandEmptyException;
import ru.ovechkin.tm.exeption.other.WrongCommandException;
import ru.ovechkin.tm.repository.CommandRepository;
import ru.ovechkin.tm.repository.ProjectRepository;
import ru.ovechkin.tm.repository.TaskRepository;
import ru.ovechkin.tm.repository.UserRepository;
import ru.ovechkin.tm.service.*;
import ru.ovechkin.tm.util.TerminalUtil;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class Bootstrap implements ServiceLocator {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);


    private final CommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);


    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);


    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);


    private final IDomainService domainService = new DomainService(taskService, projectService, userService);


    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    {
        initCommands(commandService.getCommandList());
    }

    private void initCommands(List<AbstractCommand> commandList) {
        for (AbstractCommand command : commandList) init(command);
    }

    private void init(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commands.put(command.name(), command);
        arguments.put(command.arg(), command);
    }

    public void run(final String[] args) throws Exception{
        System.out.println("** WELCOME TO TASK MANAGER **");
        if (parseArgs(args)) System.exit(0);
        process();
    }

    private void process() {
        String cmd = "";
        while (!CmdConst.CMD_EXIT.equals(cmd)) {
            System.out.print("Enter command: ");
            cmd = TerminalUtil.nextLine();
            try {
                parseCommand(cmd);
            } catch (Exception e) {
                System.err.println(e.getMessage());
                e.printStackTrace();
                System.err.println("[FAIL]");
            }
            System.out.println();
        }
    }

    private boolean parseArgs(final String[] args) throws Exception {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        parseCommand(arg);
        return true;
    }

    public void parseArg(final String arg) throws Exception {
        if (arg.isEmpty()) return;
        final AbstractCommand argument = arguments.get(arg);
        if (argument == null) throw new WrongCommandException(arg);
        argument.execute();
    }


    private void parseCommand(final String cmd) throws Exception {
        if (cmd == null || cmd.isEmpty()) throw new CommandEmptyException();
        final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new WrongCommandException(cmd);
        getAuthService().checkRoles(command.roles());
        command.execute();
    }

    public IUserService getUserService() {
        return userService;
    }

    public IAuthService getAuthService() {
        return authService;
    }

    public ICommandService getCommandService() {
        return commandService;
    }

    public ITaskService getTaskService() {
        return taskService;
    }

    public IProjectService getProjectService() {
        return projectService;
    }

    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    public IDomainService getDomainService() {
        return domainService;
    }
}