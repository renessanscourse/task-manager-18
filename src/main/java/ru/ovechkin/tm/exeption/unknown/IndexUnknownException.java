package ru.ovechkin.tm.exeption.unknown;

public class IndexUnknownException extends RuntimeException {

    public IndexUnknownException() {
        super("Error! This Index does not exist.");
    }

    public IndexUnknownException(final Integer index) {
        super("Error! This Index [" + index + "] does not exist.");
    }

}
