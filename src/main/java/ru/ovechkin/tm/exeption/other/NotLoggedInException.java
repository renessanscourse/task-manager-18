package ru.ovechkin.tm.exeption.other;

public class NotLoggedInException extends RuntimeException {

    public NotLoggedInException() {
        super("Error! You are not logged in...");
    }

}