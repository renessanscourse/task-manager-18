package ru.ovechkin.tm.command.auth;

import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.util.TerminalUtil;

public class UserUnLockCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "unlock-user";
    }

    @Override
    public String description() {
        return "Unlock user's account";
    }

    @Override
    public void execute() {
        System.out.println("[UNLOCK USER]");
        System.out.print("ENTER USER'S LOGIN: ");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().unLockUserByLogin(login);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}