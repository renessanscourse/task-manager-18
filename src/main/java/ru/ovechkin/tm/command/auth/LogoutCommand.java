package ru.ovechkin.tm.command.auth;

import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;

public class LogoutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CmdConst.LOGOUT;
    }

    @Override
    public String description() {
        return "Logout from your account";
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().logout();
        System.out.println("[LOGOUT]");
        System.out.println("[OK]");
    }

}