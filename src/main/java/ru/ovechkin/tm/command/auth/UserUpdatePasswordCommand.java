package ru.ovechkin.tm.command.auth;

import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.util.TerminalUtil;

public class UserUpdatePasswordCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CmdConst.UPDATE_PASSWORD;
    }

    @Override
    public String description() {
        return "Update password to your account";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        final User user = serviceLocator.getAuthService().findUserByUserId(userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[PASSWORD CHANGE]");
        System.out.print("ENTER YOUR CURRENT PASSWORD: ");
        final String currentPassword = TerminalUtil.nextLine();
        System.out.print("ENTER NEW PASSWORD: ");
        final String newPassword = TerminalUtil.nextLine();
        serviceLocator.getAuthService().updatePassword(currentPassword, newPassword);
    }

}