package ru.ovechkin.tm.command.auth;

import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CmdConst.REGISTRY;
    }

    @Override
    public String description() {
        return "Register new account";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.print("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        System.out.print("ENTER EMAIL: ");
        final String email = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
        System.out.println("[OK]");
    }

}