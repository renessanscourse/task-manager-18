package ru.ovechkin.tm.command.task;

import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CmdConst.TASK_UPDATE_BY_INDEX;
    }

    @Override
    public String description() {
        return "Update task by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE TASK]");
        System.out.print("ENTER TASK INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = serviceLocator.getTaskService().findTaskByIndex(userId, index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.print("ENTER NEW TASK NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW TASK DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = serviceLocator.getTaskService()
                .updateTaskByIndex(userId, index, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[COMPLETE]");
    }

}