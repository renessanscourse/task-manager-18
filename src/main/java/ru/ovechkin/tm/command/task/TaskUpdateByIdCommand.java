package ru.ovechkin.tm.command.task;

import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CmdConst.TASK_UPDATE_BY_ID;
    }

    @Override
    public String description() {
        return "Update task by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE TASK]");
        System.out.print("ENTER TASK ID: ");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findTaskById(userId, id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.print("ENTER NEW TASK NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW TASK DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = serviceLocator.getTaskService().
                updateTaskById(userId, id, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[COMPLETE]");
    }

}