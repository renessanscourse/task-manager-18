package ru.ovechkin.tm.command.system;

import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.ArgumentConst;
import ru.ovechkin.tm.constant.CmdConst;

import java.util.List;

public final class ArgumentsAllCommand extends AbstractCommand {

    @Override
    public String arg() {
        return ArgumentConst.ARG_ARGUMENTS;
    }

    @Override
    public String name() {
        return CmdConst.CMD_ARGUMENTS;
    }

    @Override
    public String description() {
        return "Show available arguments";
    }

    @Override
    public void execute() {
        final List<AbstractCommand> arguments = serviceLocator.getCommandService().getCommandList();
        for (final AbstractCommand argument : arguments) System.out.println(argument.arg());
    }

}