package ru.ovechkin.tm.command.system;

import ru.ovechkin.tm.bootstrap.Bootstrap;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.ArgumentConst;
import ru.ovechkin.tm.constant.CmdConst;

import java.util.Collection;

public final class HelpCommand extends AbstractCommand {

    @Override
    public String arg() {
        return ArgumentConst.ARG_HELP;
    }

    @Override
    public String name() {
        return CmdConst.CMD_HELP;
    }

    @Override
    public String description() {
        return "Display terminal commands";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Bootstrap bootstrap = (Bootstrap) serviceLocator;
        final Collection<AbstractCommand> commands = bootstrap.getCommands();
        for (AbstractCommand command : commands) {
            System.out.println(command.name()
                    + (command.arg() != null ? ", " + command.arg() + " - " : " - ")
                    + command.description());
        }
        System.out.println("[OK]");
    }

}
