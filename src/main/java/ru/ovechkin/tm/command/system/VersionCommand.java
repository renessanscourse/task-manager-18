package ru.ovechkin.tm.command.system;

import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.ArgumentConst;
import ru.ovechkin.tm.constant.CmdConst;

public final class VersionCommand extends AbstractCommand {

    @Override
    public String arg() {
        return ArgumentConst.ARG_VERSION;
    }

    @Override
    public String name() {
        return CmdConst.CMD_VERSION;
    }

    @Override
    public String description() {
        return "Show version info";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.8.1");
    }

}
