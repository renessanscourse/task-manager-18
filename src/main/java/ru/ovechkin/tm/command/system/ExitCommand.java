package ru.ovechkin.tm.command.system;

import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.ArgumentConst;
import ru.ovechkin.tm.constant.CmdConst;


public final class ExitCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CmdConst.CMD_EXIT;
    }

    @Override
    public String description() {
        return "Close application";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}