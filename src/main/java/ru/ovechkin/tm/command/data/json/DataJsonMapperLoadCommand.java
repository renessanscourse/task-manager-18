package ru.ovechkin.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileInputStream;

public class DataJsonMapperLoadCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-json-mapper-load";
    }

    @Override
    public String description() {
        return "Load data from json file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON LOAD]");
        final File file = new File("./dataMapper.json");
        final FileInputStream fileInputStream = new FileInputStream("./dataMapper.json");
        final ObjectMapper objectMapper = new ObjectMapper();
        final Domain domain = objectMapper.readValue(file, Domain.class);

        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getUserService().load(domain.getUsers());
        fileInputStream.close();
        System.out.println("[OK]");
    }

}