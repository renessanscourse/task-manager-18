package ru.ovechkin.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.dto.Domain;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public class DataJsonMapperSaveCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-json-mapper-save";
    }

    @Override
    public String description() {
        return "Save data to json file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON SAVE]");

        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);

        final File file = new File("./dataMapper.json");
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        final ObjectMapper objectMapper = new ObjectMapper();
        final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.close();

        System.out.println("[OK]");
    }

}