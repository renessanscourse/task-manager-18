package ru.ovechkin.tm.command.data.json;

import org.eclipse.persistence.jaxb.MarshallerProperties;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.nio.file.Files;

public class DataJsonJaxbSaveCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-json-jaxb-save";
    }

    @Override
    public String description() {
        return "Save data to json file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON SAVE]");
        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");

        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);

        final File file = new File("./dataJaxb.json");
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        jaxbMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        jaxbMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);

        jaxbMarshaller.marshal(domain, file);

        System.out.println("[OK]");
    }

}