package ru.ovechkin.tm.command.data.xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileInputStream;

public class DataXmlJaxbLoadCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-xml-jaxb-load";
    }

    @Override
    public String description() {
        return "Load data from xml file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML LOAD]");
        final File file = new File("./dataJaxb.xml");
        JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);

        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        final Domain domain = (Domain) unmarshaller.unmarshal(file);
        final FileInputStream fileInputStream = new FileInputStream("./dataJaxb.xml");

        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getUserService().load(domain.getUsers());
        fileInputStream.close();
        System.out.println("[OK]");
    }

}