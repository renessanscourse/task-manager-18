package ru.ovechkin.tm.command.data.binary;

import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.dto.Domain;

import java.io.*;
import java.nio.file.Files;

public class DataBinarySaveCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CmdConst.DATA_BIN_SAVE;
    }

    @Override
    public String description() {
        return "Save data to binary file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY SAVE]");

        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);

        final File file = new File("./data.bin");
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();

        System.out.println("[OK]");
        System.out.println();
    }

}