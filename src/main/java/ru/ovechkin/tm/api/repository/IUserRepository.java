package ru.ovechkin.tm.api.repository;

import ru.ovechkin.tm.entity.User;

import java.util.Collection;
import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    User findById(String id);

    User findByLogin(String login);

    User removeUser(User user);

    User removeById(String id);

    User removeByLogin(String login);

    User merge(User user);

    void merge(User... users);

    void merge(Collection<User> tasks);

    void load(Collection<User> tasks);

    void load(User... tasks);

    void clear();

}