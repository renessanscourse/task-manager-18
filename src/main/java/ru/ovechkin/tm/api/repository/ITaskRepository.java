package ru.ovechkin.tm.api.repository;

import ru.ovechkin.tm.entity.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskRepository {

    void add(String userId, Task task);

    void remove(String userId, Task task);

    List<Task> findAllUserTask(String userId);

    List<Task> findAllTasks();

    void clear(String userId);

    Task findById(String userId, String id);

    Task findByIndex(String userId, Integer index);

    Task findByName(String userId, String name);

    Task removeById(String userId, String id);

    Task removeByIndex(String userId, Integer index);

    Task removeByName(String userId, String name);

    void merge(Task... tasks);

    Task merge(Task task);

    void merge(Collection<Task> tasks);

    void load(Collection<Task> tasks);

    void load(Task... tasks);

    void clear();

}