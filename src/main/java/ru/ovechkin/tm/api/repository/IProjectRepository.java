package ru.ovechkin.tm.api.repository;

import java.util.Collection;
import java.util.List;

import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.Task;

public interface IProjectRepository {

    void add(String userId, Project project);

    void remove(String userId, Project project);

    List<Project> findUserProjects(String userId);

    List<Project> findAllProjects();

    void clear(String userId);

    Project findById(String userId, String id);

    Project findByIndex(String userId, Integer index);

    Project findByName(String userId, String name);

    Project removeById(String userId, String id);

    Project removeByIndex(String userId, Integer index);

    Project removeByName(String userId, String name);

    void merge(Project... projects);

    Project merge(Project project);

    void merge(Collection<Project> projects);

    void load(Collection<Project> projects);

    void load(Project... projects);

    void clear();

}