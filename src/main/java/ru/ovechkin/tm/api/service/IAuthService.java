package ru.ovechkin.tm.api.service;

import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;

public interface IAuthService {

    String getUserId();

    void checkRoles(Role[] roles);

    boolean isAuth();

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);

    User findUserByUserId(String id);

    void updateProfileInfo(
            String newLogin,
            String newFirstName,
            String newMiddleName,
            String  newLastName,
            String newEmail
    );

    void updatePassword(String currentPassword,String newPassword);

}