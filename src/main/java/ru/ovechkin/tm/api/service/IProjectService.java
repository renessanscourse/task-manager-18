package ru.ovechkin.tm.api.service;

import ru.ovechkin.tm.entity.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    void create(String userId, String name);

    void create(String userId, String name, String description);

    void add(String userId, Project project);

    void remove(String userId, Project project);

    List<Project> findUserProjects(String userId);

    List<Project> findAllProjects();

    void removeProject(String userId, Project project);

    void removeAllProjects(String userId);

    Project findProjectById(String userId, String id);

    Project findProjectByIndex(String userId, Integer index);

    Project findProjectByName(String userId, String name);

    Project updateProjectById(String userId, String id, String name, String description);

    Project updateProjectByIndex(String userId, Integer index, String name, String description);

    Project removeProjectById(String userId, String id);

    Project removeProjectByIndex(String userId, Integer index);

    Project removeProjectByName(String userId, String name);

    Project mergeOne(Project project);

    void mergeArray(Project... projects);

    void mergeCollection(Collection<Project> projects);

    void load(Collection<Project> projects);

    void load(Project... projects);

    void clear();

}