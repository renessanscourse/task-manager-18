package ru.ovechkin.tm.api.service;

import ru.ovechkin.tm.dto.Domain;

public interface IDomainService {

    void load(Domain domain);

    void export(Domain domain);

}