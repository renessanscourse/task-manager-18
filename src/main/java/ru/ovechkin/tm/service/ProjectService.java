package ru.ovechkin.tm.service;

import ru.ovechkin.tm.api.repository.IProjectRepository;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.exeption.empty.IdEmptyException;
import ru.ovechkin.tm.exeption.empty.IndexEmptyException;
import ru.ovechkin.tm.exeption.empty.NameEmptyException;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.exeption.empty.UserEmptyException;

import java.util.Collection;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name.isEmpty() || name == null) throw new NameEmptyException();
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(userId, project);
    }

    @Override
    public void create(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        final Project project = new Project();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
    }

    @Override
    public void add(final String userId, final Project project) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (project == null) return;
        projectRepository.add(userId, project);
    }

    @Override
    public void remove(final String userId, final Project project) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (project == null) return;
        projectRepository.remove(userId, project);
    }

    @Override
    public List<Project> findUserProjects(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        return projectRepository.findUserProjects(userId);
    }

    public List<Project> findAllProjects() {
        return projectRepository.findAllProjects();
    }

    @Override
    public void removeProject(final String userId, final Project project) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        projectRepository.remove(userId, project);
    }

    @Override
    public void removeAllProjects(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        projectRepository.clear(userId);
    }

    @Override
    public Project findProjectById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.findById(userId, id);
    }

    @Override
    public Project findProjectByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (index == null || index < 0) throw new IndexEmptyException();
        return projectRepository.findByIndex(userId, index);
    }

    @Override
    public Project findProjectByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return projectRepository.findByName(userId, name);
    }

    @Override
    public Project updateProjectById(
            final String userId,
            final String id,
            final String name,
            final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findProjectById(userId, id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByIndex(
            final String userId,
            final Integer index,
            final String name,
            final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (index == null || index < 0) throw new IndexEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findProjectByIndex(userId, index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project removeProjectById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.removeById(userId, id);
    }

    @Override
    public Project removeProjectByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (index == null || index < 0) throw new IndexEmptyException();
        return projectRepository.removeByIndex(userId, index);
    }

    @Override
    public Project removeProjectByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return projectRepository.removeByName(userId, name);
    }

    @Override
    public Project mergeOne(final Project project) {
        if (project == null) return null;
        projectRepository.merge(project);
        return project;
    }

    @Override
    public void mergeArray(final Project... projects) {
        for (final Project project : projects) projectRepository.merge(project);
    }

    @Override
    public void mergeCollection(final Collection<Project> projects) {
        for (final Project project : projects) projectRepository.merge(project);
    }

    @Override
    public void load(final Collection<Project> projects) {
        clear();
        projectRepository.load(projects);
    }

    @Override
    public void load(final Project... projects) {
        clear();
        projectRepository.load(projects);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

}