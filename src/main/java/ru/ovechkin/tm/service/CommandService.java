package ru.ovechkin.tm.service;

import ru.ovechkin.tm.api.repository.ICommandRepository;
import ru.ovechkin.tm.api.service.ICommandService;
import ru.ovechkin.tm.command.AbstractCommand;

import java.util.List;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return commandRepository.getCommandList();
    }

}