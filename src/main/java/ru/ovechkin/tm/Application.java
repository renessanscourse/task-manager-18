package ru.ovechkin.tm;

import ru.ovechkin.tm.bootstrap.Bootstrap;

public class Application {

    public static void main(final String[] args) throws Exception {
//        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}