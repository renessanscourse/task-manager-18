package ru.ovechkin.tm.repository;

import ru.ovechkin.tm.api.repository.IUserRepository;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserRepository implements IUserRepository {

    private List<User> users = new ArrayList<>();

    {
        users.add(new User("user", HashUtil.salt("user"), "use@mail.ck"));
        users.add(new User("admin", HashUtil.salt("admin"), Role.ADMIN));
        users.add(new User("admin1", HashUtil.salt("admin1"), Role.ADMIN));
    }

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User add(final User user) {
        users.add(user);
        return user;
    }

    @Override
    public User findById(final String id) {
        for (final User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User findByLogin(final String login) {
        for (final User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User removeUser(final User user) {
        users.remove(user);
        return user;
    }

    @Override
    public User removeById(final String id) {
        final User user = findById(id);
        if (user == null) return null;
        return removeUser(user);
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return removeUser(user);
    }

    @Override
    public User merge(final User user) {
        users.add(user);
        return user;
    }

    @Override
    public void merge(final User... users) {
        if (users == null) return;
        for (final User user : users) merge(user);
    }

    @Override
    public void merge(final Collection<User> users) {
        if (users == null) return;
        for (final User user : users) merge(user);
    }

    @Override
    public void load(final Collection<User> users) {
        clear();
        merge(users);
    }

    @Override
    public void load(final User... users) {
        clear();
        merge(users);
    }

    @Override
    public void clear() {
        users.clear();
    }

}
